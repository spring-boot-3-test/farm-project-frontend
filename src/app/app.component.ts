import {Component, OnInit} from '@angular/core';
import {AgChartOptions} from "ag-charts-community";
import {HttpClient, HttpErrorResponse, HttpResponse} from "@angular/common/http";
import {catchError, Observable, of} from "rxjs";

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {
    public options: AgChartOptions;

    constructor(private http: HttpClient) {
    }


    ngOnInit(): void {
        this.getData().subscribe(data => {
            this.updateOptions(data);
        });
        this.updateOptions(undefined);
    }

    private getData(): Observable<HttpResponse<any>> {
        return this.http.get<any>("http://localhost:8080/api/custom/getInfo").pipe(
            catchError((error: HttpErrorResponse) => {
                console.error('DataService getProjects error' + error?.message);
                return of([]);
            }));
    }

    updateOptions(freshData: any) {
        this.options = {
            autoSize: true,
            title: {
                text: 'Fields count per contract size on Farm',
            },
            subtitle: {
                text: 'From backend server',
            },
            legend: {position: 'bottom'},
            data: freshData,
            series: [{
                type: 'scatter',
                xKey: 'parcelSize',
                yKey: 'numberOfFields',
                marker: {
                    size: 10
                }
            }],
            axes: [
                {
                    type: 'number',
                    position: 'bottom',
                    title: {
                        text: 'Whole contract size',
                    },
                },
                {
                    type: 'number',
                    position: 'left',
                    title: {
                        text: 'Number of Fields per contract',
                    },
                },
            ]
        };
    }
}
